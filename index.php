<?php

/**
 * 
 * file server
 * 1. upload
 * 2. delete
 * 3. resize
 * 
 * nginx
 *  location ~* ^/fs/.*?\.(png|jpe?g)$ {
 *      try_files $uri /fs/index.php?fn=$uri;
 *  }
 * 
 * httpd
 *  RewriteEngine On
 *  RewriteCond %{REQUEST_FILENAME} !-f
 *  RewriteRule ^fs/.*(\.gif|\.jpe?g|\.png)$ /fs/index.php?fn=%{REQUEST_URI}?%{QUERY_STRING} [L]
 * 
 *  RewriteEngine On
 *  RewriteCond %{DOCUMENT_ROOT}/%{REQUEST_FILENAME} !-f
 *  RewriteRule ^/fs/ /fs/index.php?fn=%{REQUEST_URI}?%{QUERY_STRING} [L]
 * 
 */
define('IMAGE_DIR', __DIR__);
define('WEB_DIR', dirname(IMAGE_DIR));

try {
    $secretKey = 'key1';

    # upload: saves file sent but POST
    # must receive key based on file contents
    # saves metaile to /so/me/video.avi.json is passed in POST param
    if (!empty($_FILES['file'])) {
        $key = $_POST['key'];
        $file = (object) $_FILES['file'];
        $meta = $_POST['meta'] ?? '';
        $tmpfn = $file->tmp_name;
        if (!$key || $key != generateKey($tmpfn, $secretKey)) {
            throw new Exception('key');
        }
        $name = $file->name;
        $dir = getDir($name);
        $resfn = IMAGE_DIR . "/{$dir}/{$name}";
        if ($meta) {
            file_put_contents($resfn . '.json', $meta);
        }
        move_uploaded_file($tmpfn, $resfn);
        echo "/{$dir}/{$name}";
        die;
        # deletes file and it's thumbnails
        # must receive key based on filename
    } elseif (isset($_GET['act']) && $_GET['act'] == 'del') { #
        $key = $_POST['key'];
        $fn = $_POST['fn'];
        $dir = getDir($fn);
        $ffn = IMAGE_DIR . "/{$dir}/{$fn}";
        if (is_file($ffn)) {
            if (!$key || $key != generateKey($fn, $secretKey, 1)) {
                throw new Exception('key');
            }
            remove($fn);
            echo 'ok';
            die;
        }

        # resize - creates thumbnail of custom size
        # key is not needed
    } else {
        $fn = $_GET['fn'] ?? '';
        if (preg_match('!^(.*?)-(\d+)x(\d+)\.(png|jpg|jpeg)\??$!i', $fn, $m)) {#resizable
            $sFn = "{$m[1]}.{$m[4]}";
            $w = $m[2];
            $h = $m[3];
            if (is_file(WEB_DIR . $sFn) && $w && $h) {
                $resultFn = resize($sFn, $w, $h);
                header("Location: {$resultFn}", true, 302);
                die;
            }
        }
    }
} catch (Exception $e) {
    #var_dump($e);die; #debug
}
header('HTTP/1.0 404 Not Found', true, 404);
die;

function getDir($fn) {
    $hash = md5($fn);
    $lvl1 = substr($hash, 0, 2);
    $lvl2 = substr($hash, 2, 2);
    $full = __DIR__ . "/{$lvl1}/{$lvl2}";
    if (
        is_dir($full) ||
        mkdir(__DIR__ . "/{$lvl1}/{$lvl2}", 0755, 1)
    ) {
        return "{$lvl1}/{$lvl2}";
    } else {
        throw new Exception('dir');
    }
}

function remove(string $source) {
    $source = basename($source);
    $name = preg_replace('!\.[^\.]{3,4}$!', '', $source);
    $dir = IMAGE_DIR . '/' . getDir($source);
    $sfn = "{$dir}/{$source}";
    if (file_exists($sfn)) {
        foreach (scandir($dir) as $fn) {
            if (preg_match('!^' . preg_quote($name, '!') . '!', $fn)) {
                $file_name = "{$dir}/{$fn}";
                if (file_exists($file_name) && is_file($file_name)) {
                    unlink($file_name);
                }
            }
        }
        try {
            rmdir($dir);
            rmdir(dirname($dir));
        } catch (Exception $e) {
            
        } #if dir is not empty
    }
}

function resize(string $source, int $w, int $h = 0) {
    if (!$h) {
        $h = $w;
    }
    $sfn = WEB_DIR . $source;
    $is_png = preg_match('!\.png$!i', $source);
    $tsfn = preg_replace('!\.[^\.]{3,4}$!', "-{$w}x{$h}." . ($is_png ? 'png' : 'jpg'), $source);
    $tfn = WEB_DIR . $tsfn;
    if (!is_file($tfn)) {
        try {
            $src = imagecreatefromstring(file_get_contents($sfn));
            $trgt = imagecreatetruecolor($w, $h);
            list( $sw, $sh ) = getimagesize($sfn);
            if ($w > $sw || $h > $sh)
                throw new \Exception('source`s sizes must be greater than target`s');
            $sk = $sw / $sh;
            $tk = $w / $h;
            if ($sk > $tk) {
                $margin = ($sw - $sh * $tk);
                imagecopyresampled(
                    $trgt, $src, // destination, source
                    0, 0, $margin / 2, 0, // dstX, dstY, srcX, srcY
                    $w, $h, // dstW, dstH
                    $sw - $margin, $sh    // srcW, srcH
                );
            } else {
                $margin = ($sh - $sw / $tk);
                imagecopyresampled(
                    $trgt, $src, // destination, source
                    0, 0, 0, $margin / 2, // dstX, dstY, srcX, srcY
                    $w, $h, // dstW, dstH
                    $sw, $sh - $margin    // srcW, srcH
                );
            }
            $is_png ?
                    imagepng($trgt, $tfn) :
                    imagejpeg($trgt, $tfn);
            if (!getimagesize($tfn)) {
                throw new \Exception('unknown error while resizing image');
            }
        } catch (\Exception $e) {
            # var_dump($e);die;
            return $source;
        }
    }
    return $tsfn;
}

/**
 * Algorithm MUST be the same as at server
 * @param type $fn
 * @return string
 */
function generateKey($fn, $key, $byName = 0): string {
    $filePart = $byName ? basename($fn) : file_get_contents($fn, false, null, 0, 1000);
    return $filePart ?
        md5($filePart . $key) :
        '';
}
