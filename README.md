## INSTALL SERVER:
 
```txt
	1) apt-get install nginx
	2) apt-get install php-fpm
	3) apt-get install php7.2-gd
	4) mcedit /etc/php/7.2/fpm/php.ini
       cgi.fix_pathinfo=0
	5) /etc/init.d/php7.2-fpm restart
```

## Create SSL cert

```txt
	1) apt-get install git bc
	2) sudo git clone https://github.com/letsencrypt/letsencrypt /opt/letsencrypt
	3) cd /opt/letsencrypt
		./letsencrypt-auto certonly -a webroot --webroot-path=/usr/share/nginx/html -d example.com -d www.example.com
```

## Nginx configuration:
 
```nginx
server {
    listen *:80;
    server_name s1.msg1.ru;
    location /.well-known {
        alias /var/www/msg1/.well-known;
    }
    location / {
        rewrite ^ https://$host$request_uri? permanent;
    }
}

server {
        listen   *:443 ssl;
        root /var/www/msg1;
        index index.php;
        server_name s1.msg1.ru;
        ssl on;
        ssl_certificate /etc/letsencrypt/live/s1.msg1.ru/fullchain.pem;
        ssl_certificate_key /etc/letsencrypt/live/s1.msg1.ru/privkey.pem;
        ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
        ssl_ciphers 'HIGH:!aNULL:!MD5:!kEDH';
        ssl_session_cache    shared:SSL:10m;
        ssl_session_timeout  10m;
        ssl_prefer_server_ciphers on;
        add_header Strict-Transport-Security "max-age=31536000; includeSubdomains;";
        add_header X-Frame-Options SAMEORIGIN;
        access_log /var/log/nginx/msg1.access combined;
        location /.well-known {
            alias /var/www/msg1/.well-known;
        }
        location ~* ^/fs/.*?\.(png|jpe?g)$ {
            try_files $uri /fs/index.php?fn=$uri;
        }
        location ~ \.php$ {
            include snippets/fastcgi-php.conf;
            fastcgi_pass unix:/run/php/php7.2-fpm.sock;
        }
        location ~ /\.ht {
                deny all;
        }
}
```